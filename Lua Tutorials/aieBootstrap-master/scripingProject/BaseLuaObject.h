#pragma once

#include <lua.hpp>

class BaseLuaObject
{
public:
	BaseLuaObject();
	~BaseLuaObject();

	bool loadScript(const char* szPath);

	void registerLuaFunction(const char* szFuncName, lua_CFunction fcnFunction);

	bool callFunction(int argCount, int returnCount);

	static void setPointerVar(lua_State* pState, const char* pVarName, void* vpVal);

	static void* getPointerVar(lua_State* pState, const char* pVarName);

	void pushFloat(float fValue);
	float popFloat();

	void pushFunction(const char* szFunction);

private:
	lua_State* m_pLuaState;

};

