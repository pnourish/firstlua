#include "BaseLuaObject.h"
#include <iostream>


BaseLuaObject::BaseLuaObject()
{
	m_pLuaState = luaL_newstate();
	luaL_openlibs(m_pLuaState);

	// Pass the "self" pointer to lua so that it can be used in scripts
	setPointerVar(m_pLuaState, "self", this);
}


BaseLuaObject::~BaseLuaObject()
{
}

bool BaseLuaObject::loadScript(const char * szPath)
{
	int status = luaL_dofile(m_pLuaState, szPath);
	if (status)
	{ 
		std::cout << "Couldn't load script: " << szPath << std::endl << lua_tostring(m_pLuaState, -1);
		return false;
	} 
	
	// run the Init function in the script. 
	// Start by putting the function on the stack 
	lua_getglobal(m_pLuaState, "Init"); 
	// now execut it
	status = lua_pcall(m_pLuaState, 0, 0, NULL);
	if (status) 
	{ 
		std::cout << "Couldn't execute function Init " << lua_tostring(m_pLuaState, -1) << std::endl; 
	}
	return true;
}

void BaseLuaObject::registerLuaFunction(const char * szFuncName, lua_CFunction fcnFunction)
{
	lua_register(m_pLuaState, szFuncName, fcnFunction);
}

bool BaseLuaObject::callFunction(int argCount, int returnCount)
{
	if (!lua_isfunction(m_pLuaState, -(argCount + 1))) 
	{ 
		std::cout << "BaseLuaObject::callFunction - function is not in the correct position on the stack";
		return false;
	} 
	
	int status = lua_pcall(m_pLuaState, argCount, returnCount, NULL);
	if (status) 
	{ 
		std::cout << "Couldn't execute function " << lua_tostring(m_pLuaState, -1); 
		return false;
	} return true;
}

void BaseLuaObject::setPointerVar(lua_State * pState, const char * pVarName, void * vpVal)
{
	lua_pushlightuserdata(pState, vpVal);
	lua_setglobal(pState, pVarName);
}

void * BaseLuaObject::getPointerVar(lua_State * pState, const char * pVarName)
{
	lua_getglobal(pState, pVarName); 
	if (lua_isuserdata(pState, -1) == false) 
	{
		std::cout << "BaseLuaObject::getPointerVar: Variable is not a pointer";
		return NULL;
	}

	else 
	{ 
		void* vpVal = (void*)lua_topointer(pState, -1);
		lua_pop(pState, 1);
		return vpVal;
	}
}


void BaseLuaObject::pushFloat(float fValue)
{
	lua_pushnumber(m_pLuaState, fValue);
}

float BaseLuaObject::popFloat()
{
	if (lua_isnumber(m_pLuaState, -1) == false) 
	{ 
		int iStackSize = lua_gettop(m_pLuaState);
		std::cout << "BaseLuaObject::popFloat: Variable is not a number"; 
		return 0.0f;
	}
	else 
	{ 
		float fVal = (float)lua_tonumber(m_pLuaState, -1); 
		lua_pop(m_pLuaState, 1);
		return fVal;
	}
}

void BaseLuaObject::pushFunction(const char * szFunction)
{
	lua_getglobal(m_pLuaState, szFunction);
	if (!lua_isfunction(m_pLuaState, -1))
	{
		std::cout << "BaseLuaObject::pushFunction: variable is not a function!\n";
	}
}
