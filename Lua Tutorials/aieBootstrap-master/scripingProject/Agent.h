#pragma once
#include "BaseLuaObject.h"
#include <Box2D\Box2D.h>
#include <Renderer2D.h>

class SpriteBatch;
class Texture;

class Agent : public BaseLuaObject
{
public:
	Agent(b2World* pWorld, b2Vec2 position);
	~Agent() {};

	void update(float dt);
	void draw(aie::Renderer2D* renderer);
	const b2Vec2& getPosition() { return m_b2Body->GetPosition(); }
	
	void applyForce(const b2Vec2 &force); 
	void applyRotation(const float force); 
	
	static int luaGetPosition(lua_State* pState);
	static int luaApplyForce(lua_State* pState);
	static int luaApplyRotation(lua_State* pState); 
	
	b2Body* m_b2Body; 
	Texture* m_pTexture;
};

