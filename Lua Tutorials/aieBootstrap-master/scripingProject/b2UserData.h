#pragma once



enum EntityType
{
	ball = 0,
	goal,
	agent
};

struct  b2UserData
{
	b2UserData(EntityType type) : entityType(type) {}
	b2UserData(EntityType type, void* pData) : entityType(type), data(pData) {}
	EntityType entityType;
	void* data;
};