#include "MyContactListener.h"
#include "b2UserData.h"
#include "ScriptingApplication.h"


void MyContactListener::BeginContact(b2Contact * contact)
{
	//check if fixture A was a ball
	void* bodyUserData = contact->GetFixtureA()->GetBody()->GetUserData();
	if (bodyUserData)
	{
		b2UserData* data = static_cast<b2UserData*>(bodyUserData);
		if (data->entityType == ball)
		{
			//ScriptingApplication::instance()->doBallHit(contact->GetFixtureB()->GetBody()->GetUserData());
		}
	}

	//check if fixture B was a ball
	bodyUserData = contact->GetFixtureB()->GetBody()->GetUserData();
	if (bodyUserData) {
		b2UserData* data = static_cast<b2UserData*>(bodyUserData);
		if (data->entityType == ball)
		{
			//ScriptingApplication::instance()->doBallHit(contact->GetFixtureA()->GetBody()->GetUserData());
		}
	}
}

void MyContactListener::EndContact(b2Contact * contact)
{
}
