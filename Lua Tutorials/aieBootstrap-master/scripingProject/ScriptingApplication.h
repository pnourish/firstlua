#pragma once

#include "Application.h"
#include "Renderer2D.h"
#include "Audio.h"
#include <list>

#include "agent.h"



class b2World;
class b2Body;

class ScriptingApplication : public aie::Application {
	static ScriptingApplication* m_pInstance;
	ScriptingApplication();
public:
	static ScriptingApplication* instance();
	//ScriptingApplication();
	virtual ~ScriptingApplication();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

protected:

	aie::Renderer2D*	m_2dRenderer;
	aie::Font*			m_font;

	aie::Texture*		m_ballTexture;

	b2World*			m_world;
	b2Body*				m_soccerBall;

	std::list<Agent*> m_players;
	
	b2Body* m_leftGoal;

	bool m_bGameOver;

};
