#include "ScriptingApplication.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include <Texture.h>
#include <Box2D\Box2D.h>
#include "b2UserData.h"



ScriptingApplication* ScriptingApplication::m_pInstance = nullptr;

ScriptingApplication* ScriptingApplication::instance()
{
	if (m_pInstance == nullptr) m_pInstance = new ScriptingApplication();
	return m_pInstance;
}

ScriptingApplication::ScriptingApplication()
{
}

ScriptingApplication::~ScriptingApplication()
{

}

bool ScriptingApplication::startup()
{
	m_bGameOver = false;
	m_2dRenderer = new aie::Renderer2D();
	m_font = new aie::Font("./font/consolas.ttf", 32);
	m_ballTexture = new aie::Texture("./textures/ball.png");
	
	// Define the gravity vector 
	b2Vec2 gravity(0.0f, 0.0f);
	
	// Construct a world object, which will hold and simulate the rigid bodies
	m_world = new b2World(gravity);
	const float32 k_restitution = 0.4f;
	b2Body* ground;
		{ 
			b2BodyDef bd;
			bd.position.Set(0.0f, 0.0f);
			ground = m_world->CreateBody(&bd);

			b2EdgeShape shape; 
			b2FixtureDef sd; 
			sd.shape = &shape; 
			sd.density = 0.0f; 
			sd.restitution = k_restitution;

			// Left vertical
			shape.Set(b2Vec2(0.0f, 0.0f), b2Vec2(0.0f, 720.0f));
			ground->CreateFixture(&sd);
			
			// Right vertical 
			shape.Set(b2Vec2(1280.0f, 0.0f), b2Vec2(1280.0f, 720.0f));
			ground->CreateFixture(&sd);
			
			// Top horizontal
			shape.Set(b2Vec2(0.0f, 720.0f), b2Vec2(1280.0f, 720.0f)); 
			ground->CreateFixture(&sd); 
			
			// Bottom horizontal
			shape.Set(b2Vec2(0.0f, 0.0f), b2Vec2(1280.0f, 0.0f));
			ground->CreateFixture(&sd);
	}
		// Define the dynamic body. We set its position and call the body factory.
		b2BodyDef bodyDef; 
		bodyDef.type = b2_dynamicBody; 
		bodyDef.position.Set(640.0f, 360.0f);
		m_soccerBall = m_world->CreateBody(&bodyDef);
		m_soccerBall->SetUserData(new b2UserData(ball));

		// Define another box shape for our dynamic body 
		b2PolygonShape dynamicBox;
		dynamicBox.SetAsBox(24.0f, 24.0f); 
		
		// define the dynamic body fixture 
		b2FixtureDef fixtureDef; fixtureDef.shape = &dynamicBox;
		
		// Set the box density to be non-zero, so it will be dynamic
		fixtureDef.density = 1.0f;
		
		// Override the default friction
		fixtureDef.friction = 0.3f; 

		// Add the shape to the body 
		m_soccerBall->CreateFixture(&fixtureDef); 
		m_soccerBall->SetLinearVelocity(b2Vec2(10, -15));
		
		
		
		// load a soccer player
		Agent* agent = new Agent(m_world, b2Vec2(100, 100));
		if (agent->loadScript("./scripts/agent.lua") == false) 
		{ 
			delete agent;
			return false;
		} 

		agent->m_b2Body->SetUserData(new b2UserData(EntityType::agent, (void*)agent));
		
		m_players.push_back(agent);
		


		// left goal
		{
			b2BodyDef bd;
			bd.position.Set(100.0f, 360.0f);
			m_leftGoal = m_world->CreateBody(&bd);

			b2EdgeShape shape;

			b2FixtureDef sd;
			sd.shape = &shape;
			sd.density = 0.0f;
			sd.restitution = k_restitution;

			// Left vertical
			shape.Set(b2Vec2(0.0f, -50.0f), b2Vec2(0.0f, 50.0f));
			m_leftGoal->CreateFixture(&sd);

			m_leftGoal->SetUserData(new b2UserData(goal));
		}
		
		
		
		return true;
}

void ScriptingApplication::shutdown()
{
	m_world->DestroyBody(m_soccerBall);
	delete m_world;
	delete m_ballTexture;
	delete m_font;
	delete m_2dRenderer;
}

void ScriptingApplication::update(float deltaTime)
{

	// input example 
	aie::Input* input = aie::Input::getInstance(); 
	
	// Prepare for simulation. Typically we use a time step of 1/60 of a 
	// second (60Hz) and 10 iterations. This provides a high quality simulation
	// in most game scenarios. 
	static float32 timeStep = 1.0f / 60.0f;
	static int32 velocityIterations = 6;
	static int32 positionIterations = 2;
	
	// Instruct the world to perform a single step of simulation 
	// It is generall best to keep the time step and interations fixed 
	m_world->Step(timeStep, velocityIterations, positionIterations);
	
	// exit the application 
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE)) 
		quit();

	for (auto agent : m_players)
	{
		agent->update(deltaTime);
	}

}

void ScriptingApplication::draw()
{

	// wipe the screen to the background colour 
	clearScreen(); 
	
	// begin drawing sprites
	m_2dRenderer->begin(); 
	
	// draw your stuff here!
	b2Vec2 position = m_soccerBall->GetPosition(); 
	float angle = m_soccerBall->GetAngle();
	m_2dRenderer->drawSprite(m_ballTexture, position.x, position.y, 0, 0, angle);
	

	for (auto agent : m_players) 
	{ 
		agent->draw(m_2dRenderer);
	}


	position = m_leftGoal->GetPosition();
	m_2dRenderer->setRenderColour(1, 0, 0, 1);
	m_2dRenderer->drawSprite(nullptr, position.x, position.y, 10, 100);

	m_2dRenderer->setRenderColour(1, 1, 1, 1);

	// output some text
	m_2dRenderer->drawText(m_font, "Press ESC to quit", 0, 0); 
	

	if (m_bGameOver)
	{
		m_2dRenderer->drawText(m_font, "Gooooooooooooooooooooal!!!1", 200, 700);
	}

	// done drawing sprites 
	m_2dRenderer->end();
}
